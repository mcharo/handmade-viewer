﻿$signature = @"
	
	[DllImport("user32.dll")]  
	public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);  
	public static IntPtr FindWindow(string windowName){
		return FindWindow(null,windowName);
	}
	[DllImport("user32.dll")]
	public static extern bool SetWindowPos(IntPtr hWnd, 
	IntPtr hWndInsertAfter, int X,int Y, int cx, int cy, uint uFlags);
	[DllImport("user32.dll")]  
	public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow); 
	static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
	static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
	const UInt32 SWP_NOSIZE = 0x0001;
	const UInt32 SWP_NOMOVE = 0x0002;
	const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
	public static void MakeTopMost (IntPtr fHandle)
	{
		SetWindowPos(fHandle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
	}
	public static void MakeNormal (IntPtr fHandle)
	{
		SetWindowPos(fHandle, HWND_NOTOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
	}
"@


Add-Type -MemberDefinition $signature -Name API -Namespace Win32

function Set-TopMost
{
	param(		
		[Parameter(Position=0,ValueFromPipelineByPropertyName=$true)]
        [Alias('MainWindowHandle')]
        $hWnd=0,

		[Parameter()][switch]$Disable
	)
	
	if($hWnd -ne 0)
	{
		if($Disable)
		{
			Write-Verbose "Set process handle :$hWnd to NORMAL state"
			$null = [Win32.API]::MakeNormal($hWnd)
			return
		}
		
		Write-Verbose "Set process handle :$hWnd to TOPMOST state"
		$null = [Win32.API]::MakeTopMost($hWnd)
	}
	else
	{
		Write-Verbose "$hWnd is 0"
	}
}

function New-IE
{
    [CmdletBinding()]
    param(
        $Url,
        [switch]$TopMost
    )
    $InternetExplorer = New-Object -ComObject InternetExplorer.Application
    $InternetExplorer.MenuBar = $false
    $InternetExplorer.Toolbar = 0
    $InternetExplorer.StatusBar = $false
    $InternetExplorer.Visible = $true
    $InternetExplorer.Navigate2($Url)
    if ($TopMost)
    {
        if ($InternetExplorer.HWND)
        {
            Set-TopMost -hWnd $InternetExplorer.HWND
        }
        else
        {
            Write-Warning "Not able to retrieve IE window handle, unable to set window as topmost"
        }
    }
}

function Get-HandmadeHeroYouTubeUrl
{
    [CmdletBinding()]
    param(
        [Parameter(ValueFromPipeline=$true)]
        [int]$Day
    )
    process {
        $TemplateUrl = 'https://hero.handmade.network/episode/code/day{0}/'
        $DayString = $Day.ToString('000')
        $HandmadeUrl = $TemplateUrl -f $DayString
        $HandMadeResponse = Invoke-WebRequest -Uri $HandmadeUrl
        $MetaTags = $HandmadeResponse.ParsedHtml.documentElement.getElementsByTagName('meta')
        $EmbedUrl = $MetaTags | Where-Object { $_.getAttribute('property') -eq 'og:video:url' } | Foreach-Object { $_.getAttribute('content') }
        $EmbedUrl
    }
}

function Get-HandmadeHero
{
    [CmdletBinding()]
    param(
        [int]$Day
    )
    
    
    $EmbedUrls = $Day..$($Day + 5) | Get-HandmadeHeroYouTubeUrl
    $EmbedJson = $EmbedUrls | ConvertTo-Json -Compress
    $EmbedCode = "<html>
        <head>
            <title>Handmade Viewer</title>
            <script>
            var index = 0;
            var urls = $EmbedJson;
            function next_day()
            {
                index++;
                document.getElementById('handmade-viewer').src = urls[index];
                console.log('index: ' + index);
                document.getElementById('prev-day-link').style.visibility = 'visible';
                if (index >= urls.length)
                {
                    document.getElementById('next-day-link').style.visibility = 'hidden';
                }
            }
            function prev_day()
            {
                if (index > 0)
                {
                    --index;
                    console.log('index: ' + index);
                    document.getElementById('handmade-viewer').src = urls[index];
                    if (index === 0)
                    {
                        document.getElementById('prev-day-link').style.visibility = 'hidden';
                    }
                }
            }
            </script>
            <style>
            a {
                font-family: sans-serif;
                font-size: 1.5em;
                font-variant: small-caps;
                text-decoration: none;
            }
            .left {
                float: left;
            }
            .right {
                float: right;
            }
            </style>
        </head>
        <body>
            <iframe id='handmade-viewer' width='100%' height='97%' src='$($EmbedUrls[0])' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>
            <a class='left' id='prev-day-link' style='visibility: hidden' href='#' onclick='prev_day()'>Previous Day</a>
            <a class='right' id='next-day-link' href='#' onclick='next_day()'>Next Day</a>
        </body>
    </html>"
    $EmbedCode | Set-Content -Path $env:temp\day.html
    New-IE -Url $env:temp\day.html -TopMost
}